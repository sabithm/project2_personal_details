#! /usr/bin/env python

import rospy

import actionlib

import person_details.msg


class PersonAction(object):
    # create messages that are used to publish feedback/result
    _feedback = person_details.msg.personFeedback()
    _result = person_details.msg.personResult()

    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name,person_details.msg.personAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
      
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        
        
        # publish info to the console for the user
        rospy.loginfo('performing data structure %s',goal.names)
        x=goal.names
              
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
           
            # publish the feedback
        
        #create dictionary for person details   
        p={'sabith':21,'thoufeeque':22,'rulshid':23,'archana':50,'sandra':66}
        ## start executing the action
        for i in p:
            flag=0
            if i==x:
                flag=1
                person_age=p[i]
                break
        if flag==1:
           self._feedback.age=person_age
           self._as.publish_feedback(self._feedback)
        else:
           self._feedback.age=0
           self._as.publish_feedback(self._feedback)
                  
        # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes
        r.sleep()
          
        if success:
            #action server notifies the action client that the goal is complete
            self._result.age = self._feedback.age
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)

#start of main function        
if __name__ == '__main__':
    
    rospy.init_node('person_details')
    # creates the action server
    server = PersonAction(rospy.get_name())
    rospy.spin()

       
